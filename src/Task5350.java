import s50.Account;
public class Task5350 {
    public static void main(String[] args) throws Exception {
        //task 3
        Account account1 = new Account("100001", "Current Account");
        Account account2 = new Account("200001", "Deposit Account", 1000);
        System.out.println("Account 1" + account1);
        System.out.println("Account 2" + account2);
        //task 4
        account1.credit(2000);
        account2.credit(3000);
        System.out.println("Task 4 - Account 1" + account1);
        System.out.println("Task 4 - Account 2" + account2);
        //task 5
        account1.debit(1000);
        account2.debit(5000);
        System.out.println("Task 5 - Account 1" + account1);
        System.out.println("Task 5 - Account 2" + account2);
        //task 6
        account1.transferTo(account2, 2000);
        System.out.println("Task 6 - Account 1" + account1);
        System.out.println("Task 6 - Account 2" + account2);
        //task 7
        account2.transferTo(account1, 2000);
        System.out.println("Task 7 - Account 1" + account1);
        System.out.println("Task 7 - Account 2" + account2);
    }
}
